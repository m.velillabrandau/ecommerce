<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//AUTH
Route::get('/login', 'Auth\LoginController@showLoginForm');

//WEB
Route::get('/', 'WebController@index')->name('indexWeb');
Route::get('shop', 'WebController@shop')->name('shop');

Route::post('login', 'Auth\LoginController@login')->name('login');

Route::post('logout', 'Auth\LoginController@logout')->name('logout');

//LOBBY
Route::get('lobby', 'LobbyController@index')->name('lobby');

//PRODUCTOS
Route::post('agregarProducto', 'LobbyController@agregarProducto')->name('agregarProducto');
Route::post('eliminarProducto', 'LobbyController@eliminarProducto')->name('eliminarProducto');

//SHOP
Route::get('sortByRankPrice', 'ShopController@sortByRankPrice')->name('sortByRankPrice');










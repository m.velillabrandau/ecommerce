
@extends('layoutweb')

@section('contentweb')

	<!-- breadcrumb area start -->
	{{-- <div class="breadcrumb-area">
	    <div class="container">
	        <div class="row">
	            <div class="col-12">
	                <div class="breadcrumb-wrap">
	                    <nav aria-label="breadcrumb">
	                        <ul class="breadcrumb">
	                            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
	                            <li class="breadcrumb-item"><a href="shop-grid-left-sidebar.html">shop</a></li>
	                            <li class="breadcrumb-item active" aria-current="page">shop grid left sidebar</li>
	                        </ul>
	                    </nav>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> --}}
	<!-- breadcrumb area end -->

	<!-- page main wrapper start -->
	<main>
	    <div class="shop-main-wrapper pt-100 pb-100 pt-sm-58 pb-sm-58">
	        <div class="container">
	            <div class="row">
	                <div class="col-xl-3 col-lg-4 order-2 order-lg-1">
	                    <div class="sidebar-wrapper mt-md-100 mt-sm-48">
	                        <!-- single sidebar start -->
	                        <div class="sidebar-single">
	                            <div class="sidebar-title">
	                                <h3>Tienda</h3>
	                            </div>
	                            <div class="sidebar-body">
	                                <ul class="sidebar-category"><!-- CATEGORIAS y SUB CATEGORIAS -->
	                                    <li><a href="#">Accesorios</a>
	                                        <ul class="children">
	                                            <li><a href="#">Aros</a></li>
	                                            <li><a href="#">Pendientes</a></li>
	                                            <li><a href="#">Anillos</a></li>
	                                        </ul>
	                                    </li>
	                                </ul>
	                            </div>
	                        </div>
	                        <!-- single sidebar end -->

	                        <!-- single sidebar start -->
							<!-- CATEGORIAS -->
	                        <!-- <div class="sidebar-single"> 
	                            <div class="sidebar-title">
	                                <h3>category</h3>
	                            </div>
	                            <div class="sidebar-body">
	                                <ul class="price-container">
	                                    <li class="active"> 
	                                        <label class="checkbox-container">
	                                            Hombre
	                                            <input class="checkmark-category" type="checkbox" name="checkbox" data-category="1">
	                                            <span class="checkmark"></span>
	                                        </label>
	                                    </li>
	                                    <li> 
	                                        <label class="checkbox-container">
	                                            Mujer
	                                            <input class="checkmark-category" type="checkbox" name="checkbox" data-category="2">
	                                            <span class="checkmark"></span>
	                                        </label>
	                                    </li>
	                                </ul>
	                            </div>
	                        </div> -->
	                        <!-- single sidebar end -->

	                        <!-- single sidebar start -->
	                        <div class="sidebar-single">
	                            <div class="sidebar-title">
	                                <h3>precio</h3>
	                            </div>
	                            <div class="sidebar-body">
	                                <ul class="price-container">
	                                    <li class=""> 
	                                        <label class="radio-container">
	                                            $0 - $5000
	                                            <input id="input-price-0" class="checkmark-price" type="radio" name="radio" data-low="0" data-high="5000">
	                                            <span class="checkmark"></span>
	                                        </label>
	                                    </li>
	                                    <li> 
	                                        <label class="radio-container">
	                                            $5000 - $10000
	                                            <input id="input-price-5000" class="checkmark-price" type="radio" name="radio" data-low="5000" data-high="10000">
	                                            <span class="checkmark"></span>
	                                        </label>
	                                    </li>
	                                    <li> 
	                                        <label class="radio-container">
	                                            $10000 - $15000
	                                            <input id="input-price-10000" class="checkmark-price" type="radio" name="radio" data-low="10000" data-high="15000">
	                                            <span class="checkmark"></span>
	                                        </label>
	                                    </li>
	                                    <li> 
	                                        <label class="radio-container">
                                                $15000 - $ +
	                                            <input id="input-price-15000" class="checkmark-price" type="radio" name="radio" data-low="15000" data-high="+">
	                                            <span class="checkmark"></span>
	                                        </label>
	                                    </li>
	                                </ul>
	                            </div>
	                        </div>
	                        <!-- single sidebar end -->

	                        <!-- single sidebar start -->
	                        <div class="sidebar-single">
	                            <div class="sidebar-title">
	                                <h3>popular product</h3>
	                            </div>
	                            <div class="sidebar-body">
	                                <div class="popular-item-inner">
	                                    <div class="popular-item">
	                                        <div class="pop-item-thumb">
	                                            <a href="product-details.hrtml">
	                                                <img src="melani/assets/img/product/product-6.jpg" alt="">
	                                            </a>
	                                        </div>
	                                        <div class="pop-item-des">
	                                            <h4><a href="product-details.html">Arbor Swoon Camber</a></h4>
	                                            <span class="pop-price">$50.00</span>
	                                        </div>
	                                    </div> <!-- end single popular item -->
	                                    <div class="popular-item">
	                                        <div class="pop-item-thumb">
	                                            <a href="product-details.hrtml">
	                                                <img src="melani/assets/img/product/product-7.jpg" alt="">
	                                            </a>
	                                        </div>
	                                        <div class="pop-item-des">
	                                            <h4><a href="product-details.html">Arbor Swoon Camber</a></h4>
	                                            <span class="pop-price">$50.00</span>
	                                        </div>
	                                    </div> <!-- end single popular item -->
	                                    <div class="popular-item">
	                                        <div class="pop-item-thumb">
	                                            <a href="product-details.hrtml">
	                                                <img src="melani/assets/img/product/product-8.jpg" alt="">
	                                            </a>
	                                        </div>
	                                        <div class="pop-item-des">
	                                            <h4><a href="product-details.html">Arbor Swoon Camber</a></h4>
	                                            <span class="pop-price">$50.00</span>
	                                        </div>
	                                    </div> <!-- end single popular item -->
	                                </div>
	                            </div>
	                        </div>
	                        <!-- single sidebar end -->

	                        <!-- single sidebar start -->
	                        <div class="sidebar-single">
	                            <div class="advertising-thumb img-full fix">
	                                <a href="#">
	                                    <!-- <img src="melani/assets/img/banner/advertising.jpg" alt=""> -->
	                                </a>
	                            </div>
	                        </div>
	                        <!-- single sidebar end -->
	                    </div>
	                </div>
	                <!-- product view wrapper area start -->
	                <div class="col-xl-9 col-lg-8 order-1 order-lg-2">
	                    <div class="shop-product-wrapper">
	                        <!-- shop product top wrap start -->
	                        <div class="shop-top-bar">
	                            <div class="row">
	                                <div class="col-lg-7 col-md-6">
	                                    <div class="top-bar-left">
	                                        <div class="product-view-mode">
	                                            <a class="active" href="#" data-target="grid"><i class="fa fa-th"></i></a>
	                                            {{-- <a href="#" data-target="list"><i class="fa fa-list"></i></a> --}}
	                                        </div>
	                                        <div class="product-amount">
	                                            {{-- <p>Showing 1–16 of 21 results</p> --}}
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-lg-5 col-md-6">
	                                    <div class="top-bar-right">
	                                        <div class="product-short">
	                                            <p>Ordenar por: </p>
	                                            <select id="order-product"class="nice-select" name="sortby">
	                                                <!-- <option value="trending">Relevance</option>
	                                                <option value="sales">Name (A - Z)</option>
	                                                <option value="sales">Name (Z - A)</option> -->
	                                                <option id="" class="" value="" selected disabled>Seleccione orden...</option>
	                                                <option id="order-asc" class="" value="ASC">Precio (Bajo &gt; Alto)</option>
	                                                <option id="order-desc" class="" value="DESC">Precio (Alto &gt; Bajo)</option>
	                                                <!-- <option value="date">Rating (Lowest)</option>
	                                                <option value="price-asc">Model (A - Z)</option>
	                                                <option value="price-asc">Model (Z - A)</option> -->
	                                            </select>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- shop product top wrap start -->
	                        <!-- product view mode wrapper start -->
	                        <div class="shop-product-wrap grid row">
								<?php foreach ($productos as $key => $producto) { ?>
									
		                            <div class="col-xl-4 col-lg-6 col-md-4 col-sm-6">
		                                <!-- product grid item start -->
		                                <div class="product-item mb-20">
		                                    <div class="product-thumb" style="width: 250px; height: 350px;">
		                                        <a >
		                                        	<?php if($producto->file != '' || $producto->file != null){ ?>
		                                            	<img style="height: 100%; width: 100%;" src="images/products/<?= $producto->file ?>" alt="product image">
		                                            <?php }else { ?>
		                                            	<img style="height: 100%; width: 100%;" src="melani/assets/img/product/product-1.jpg" alt="product image">
		                                            <?php } ?>
		                                        </a>
		                                        <div class="box-label">
		                                        	<?php if($producto->new == 1){ ?>
			                                            <div class="product-label new">
			                                                <span>Nuevo</span>
			                                            </div>
		                                            <?php } ?>
		                                            <?php if($producto->discount > 0){ ?>
			                                            <div class="product-label discount">
			                                                <span><?= $producto->discount ?>%</span>
			                                            </div>
		                                            <?php } ?>
		                                        </div>
		                                        <div class="product-action-link">
		                                            <a href="#" data-toggle="modal" data-target="#quick_view"> <span
		                                                data-toggle="tooltip" data-placement="left" title="Detalles"><i class="ion-ios-eye-outline"></i></span> </a>
		                                            {{-- <a href="#" data-toggle="tooltip" data-placement="left" title="Compare"><i
		                                                class="ion-ios-loop"></i></a>
		                                            <a href="#" data-toggle="tooltip" data-placement="left" title="Wishlist"><i
		                                                class="ion-ios-shuffle"></i></a> --}}
		                                        </div>
		                                    </div>
		                                    <div class="product-description text-center">
		                                        <div class="manufacturer">
		                                            <p><a href="product-details.html"><?= $producto->brand ?></a></p>
		                                        </div>
		                                        <div class="product-name">
		                                            <h3><a href="product-details.html"><?= $producto->name ?></a></h3>
		                                        </div>
		                                        <div class="price-box">
		                                        	<?php if($producto->discount > 0 ){ ?>
				                                        <span class="regular-price">$<?= ($producto->price * (100 - $producto->discount))/100 ?></span>
				                                            <span class="old-price"><del>$<?= $producto->price  ?></del></span>

				                                    <?php }else{ ?>
				                                        <span class="regular-price">$<?= $producto->price ?></span>
				                                    <?php } ?>
		                                        </div>
		                                        <!-- <div class="product-btn">
		                                            <a href="#"><i class="ion-bag"></i>Add to cart</a>
		                                        </div> -->
{{-- 		                                        <div class="hover-box text-center">
		                                            <div class="ratings">
		                                                <span><i class="fa fa-star"></i></span>
		                                                <span><i class="fa fa-star"></i></span>
		                                                <span><i class="fa fa-star"></i></span>
		                                                <span><i class="fa fa-star"></i></span>
		                                                <span><i class="fa fa-star"></i></span>
		                                            </div>
		                                        </div> --}}
		                                    </div>
		                                </div>
		                                <!-- product grid item end -->
		                                <!-- product list item start -->
		                                <div class="product-list-item mb-20">
		                                    <div class="product-thumb">
		                                        <a href="product-details.html">
		                                            <?php if($producto->file != '' || $producto->file != null){ ?>
		                                            	<img style="height: 100%; width: 100%;" src="images/products/<?= $producto->file ?>" alt="product image">
		                                            <?php }else { ?>
		                                            	<img style="height: 100%; width: 100%;" src="melani/assets/img/product/product-1.jpg" alt="product image">
		                                            <?php } ?>
		                                        </a>
		                                        <div class="box-label">
		                                        	<?php if($producto->new == 1){ ?>
			                                            <div class="product-label new">
			                                                <span>new</span>
			                                            </div>
		                                            <?php } ?>
		                                            <?php if($producto->discount > 0){ ?>
			                                            <div class="product-label discount">
			                                                <span><?= $producto->discount ?>%</span>
			                                            </div>
		                                            <?php } ?>
		                                        </div>
		                                        <div class="product-action-link">
		                                            <a href="#" data-toggle="modal" data-target="#quick_view"> <span
		                                                data-toggle="tooltip" data-placement="left" title="Detalles"><i class="ion-ios-eye-outline"></i></span> </a>
		                                            {{-- <a href="#" data-toggle="tooltip" data-placement="left" title="Compare"><i
		                                                class="ion-ios-loop"></i></a>
		                                            <a href="#" data-toggle="tooltip" data-placement="left" title="Wishlist"><i
		                                                class="ion-ios-shuffle"></i></a> --}}
		                                        </div>
		                                    </div>
		                                    <div class="product-list-content">
		                                        <h4><a href="#"><?= $producto->brand  ?></a></h4>
		                                        <h3><a href="product-details.html"><?= $producto->name ?></a></h3>
		                                        {{-- <div class="ratings">
		                                            <span class="good"><i class="fa fa-star"></i></span>
		                                            <span class="good"><i class="fa fa-star"></i></span>
		                                            <span class="good"><i class="fa fa-star"></i></span>
		                                            <span class="good"><i class="fa fa-star"></i></span>
		                                            <span><i class="fa fa-star"></i></span>
		                                        </div> --}}
		                                        <div class="pricebox">
		                                        	<?php if($producto->discount > 0 ){ ?>
				                                        <span class="regular-price">$<?= ($producto->price * (100 - $producto->discount))/100 ?></span>
				                                            <span class="old-price"><del>$<?= $producto->price  ?></del></span>

				                                    <?php }else{ ?>
				                                        <span class="regular-price">$<?= $producto->price ?></span>
				                                    <?php } ?>
		                                        </div>
		                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Phasellus id nisi quis justo tempus mollis sed et dui. In hac habitasse platea dictumst. Suspendisse ultrices mauris diam. Nullam sed aliquet elit.</p>
		                                        <!-- <div class="product-btn product-btn__color">
		                                            <a href="#"><i class="ion-bag"></i>Add to cart</a>
		                                        </div> -->
		                                    </div>
		                                </div>
		                                <!-- product list item end -->
		                            </div>
		                        <?php } ?>
	                        </div>
	                        <!-- product view mode wrapper start -->
	                    </div>
	                    <!-- start pagination area -->
	                    <div class="paginatoin-area text-center mt-18">
	                        <ul class="pagination-box">
	                            <li><a class="Previous" href="{{ $productos->previousPageUrl() }}">Anterior</a></li>
<!-- 	                        <li class="active"><a href="#">1</a></li>
	                            <li><a href="#">2</a></li>
	                            <li><a href="#">3</a></li> -->
	                            <li><a class="Next" href="{{ $productos->nextPageUrl() }}">Siguiente</a></li>
	                        </ul>
	                    </div>
	                    <!-- end pagination area -->
	                </div>
	            </div>
	        </div>
	    </div>
	</main>
	<!-- page main wrapper end -->

@stop

<!-- Jquery Min Js -->
<!-- <script src="assets/js/vendor/jquery-3.3.1.min.js"></script> -->
<script src="{{ asset('melani/assets/js/vendor/jquery-3.3.1.min.js') }}"></script>

<script>
    $(function () {

    	// CUANDO CARGA LA PAGINA WEB
    	var url_string = window.location.href
		var url = new URL(url_string);
		
		var category = url.searchParams.get("category");
		if(category != null){
			let test = $('.checkmark-category').find().attr('data-category', category);
			console.log(test);
		}

		var rankPrice = url.searchParams.get("rankPrice");

		if(rankPrice != null){
			console.log("PRECIO:", rankPrice);
			var array = rankPrice.split("-", 2);
			console.log("ARRAY: ", array[0]);
			$('#input-price-'+array[0]).attr("checked", "checked");
		}

		var order = url.searchParams.get("order");
		if(order != null){
			$("#order-product option:selected").remove();
			order = order.toLowerCase();
			console.log("ORDEN:", order);
			$('#order-'+order).attr("selected", "selected");
		}

		// FUNCIONES
    	$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});

	    $('.checkmark-price').change(function() {

	    	let minPrice = $(this).attr('data-low');
	    	let maxPrice = $(this).attr('data-high');
	    	let rankPrice = minPrice+'-'+maxPrice
	    	insertParam("rankPrice",rankPrice);


	    });

	    $('.checkmark-category').change(function() {

	    	let category = $(this).attr('data-category');
	    	insertParam("category",category);


	    });

	    $('.nice-select').change(function(event) {

	    	let order = $(this).find(":selected").val();
	    	console.log("ORDEN: ", order);
	    	insertParam("order",order);
	    });


	    function insertParam(key, value)
		{
		    key = encodeURI(key); value = encodeURI(value);

		    var kvp = document.location.search.substr(1).split('&');

		    var i=kvp.length; var x; while(i--) 
		    {
		        x = kvp[i].split('=');

		        if (x[0]==key)
		        {
		            x[1] = value;
		            kvp[i] = x.join('=');
		            break;
		        }
		    }

		    if(i<0) {kvp[kvp.length] = [key,value].join('=');}

		    //this will reload the page, it's likely better to store this until finished
		    document.location.search = kvp.join('&'); 
		}

    });
</script>
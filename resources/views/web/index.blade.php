@extends('layoutweb')

@section('contentweb')

    <?php $aux = 1; ?>
    <?php if($aux != 1){ ?>
        <!-- slider area start -->
        <div class="hero-area">
            <div class="hero-slider-active slider-arrow-style slick-dot-style hero-dot">
                <div class="hero-single-slide hero-2 hero-2__style-4 d-flex align-items-center" style="background-image: url(melani/assets/img/slider/slide-7.jpg);">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="slider-content text-center">
                                    <h3>melani bags 2018</h3>
                                    <h1>big winter sale<br>50% off</h1>
                                    <a href="shop-grid-left-sidebar.html" class="slider-btn slider-btn__2">shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hero-single-slide hero-2 hero-2__style-4 d-flex align-items-center" style="background-image: url(melani/assets/img/slider/slide-8.jpg);">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="slider-content text-center ml-auto">
                                    <h3>december 15-30, 2018</h3>
                                    <h1>run way mega sale<br>up to 80% off</h1>
                                    <a href="shop-grid-left-sidebar.html" class="slider-btn slider-btn__2">shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider area end -->

        <!-- featured product area start -->
        <div class="page-section pt-100 pb-14 pt-sm-60 pb-sm-0">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title text-center pb-44">
                            <p>The latest featured products</p>
                            <h2>Featured products</h2>
                        </div>
                    </div>
                </div>
                <div class="row product-carousel-one spt slick-arrow-style" data-row="2">

                    <?php foreach ($productos as $key => $producto) { ?>

                        <div class="col">
                            <div class="product-item mb-20">
                                <div class="product-thumb">
                                    <a href="product-details.html">
                                        <img src="melani/assets/img/product/product-ac1.jpg" alt="product image">
                                    </a>
                                    <div class="box-label">
                                        <?php if($producto->new == 1){ ?>
                                            <div class="product-label new">
                                                <span>new</span>
                                            </div>
                                        <?php } ?>
                                        <?php if($producto->discount > 0){ ?>
                                            <div class="product-label discount">
                                                <span><?= $producto->discount  ?></span>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="product-action-link">
                                        <a href="#" data-toggle="modal" data-target="#quick_view"> <span
                                            data-toggle="tooltip" data-placement="left" title="Quick view"><i class="ion-ios-eye-outline"></i></span> </a>
                                        <a href="#" data-toggle="tooltip" data-placement="left" title="Compare"><i
                                            class="ion-ios-loop"></i></a>
                                        <a href="#" data-toggle="tooltip" data-placement="left" title="Wishlist"><i
                                            class="ion-ios-shuffle"></i></a>
                                    </div>
                                </div>
                                <div class="product-description text-center">
                                    <div class="manufacturer">
                                        <p><a href="product-details.html"><?= $producto->brand  ?></a></p>
                                    </div>
                                    <div class="product-name">
                                        <h3><a href="product-details.html"><?= $producto->name  ?></a></h3>
                                    </div>
                                    <div class="price-box">
                                        <?php if($producto->discount > 0 ){ ?>
                                            <span class="regular-price">$<?= ($producto->price * (100 - $producto->discount))/100 ?></span>
                                                <span class="old-price"><del>$<?= $producto->price  ?></del></span>

                                        <?php }else{ ?>
                                            <span class="regular-price">$<?= $producto->price ?></span>
                                        <?php } ?>
                                    </div>
                                    <!-- <div class="product-btn">
                                        <a href="#"><i class="ion-bag"></i>Add to cart</a>
                                    </div> -->
                                    <div class="hover-box text-center">
                                        <div class="ratings">
                                            <span><i class="fa fa-star"></i></span>
                                            <span><i class="fa fa-star"></i></span>
                                            <span><i class="fa fa-star"></i></span>
                                            <span><i class="fa fa-star"></i></span>
                                            <span><i class="fa fa-star"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php }?>

                </div>
            </div>
        </div>
        <!-- featured product area end -->

        <!-- banner statistics 04 start -->
        <div class="banner statistic-four">
            <div class="container">
                <div class="row">
                    <div class="col1 col-sm-6">
                        <div class="img-container img-full fix">
                            <a href="#">
                                <img src="melani/assets/img/banner/cms_4.1.jpg" alt="banner image">
                            </a>
                        </div>
                    </div>
                    <div class="col3 col">
                        <div class="img-container img-full fix mb-30">
                            <a href="#">
                                <img src="melani/assets/img/banner/cms_4.2.jpg" alt="banner image">
                            </a>
                        </div>
                        <div class="img-container img-full fix">
                            <a href="#">
                                <img src="melani/assets/img/banner/cms_4.3.jpg" alt="banner image">
                            </a>
                        </div>
                    </div>
                    <div class="col2 col-sm-6">
                        <div class="img-container img-full fix mt-sm-30">
                            <a href="#">
                                <img src="melani/assets/img/banner/cms_4.4.jpg" alt="banner image">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner statistics 04 end -->

        <!-- catrgory product area start -->
        <div class="category-product-area pt-98 pt-lg-94 pt-md-94 pb-14 pt-sm-26 pb-sm-0">
            <div class="container">
                <div class="row">

                    <div class="col-lg-6">
                        <div class="new-arrivals-area">
                            <div class="section-title-2 mb-60 mb-sm-40">
                                <h3>new arrivals</h3>
                            </div>
                            <div class="category-active row slick-arrow-style spt arrow-top">
                                <?php if(!empty($productosNuevos)){ ?>
                                    <?php foreach ($productosNuevos as $key => $productoNuevo) { ?>
                                        <div class="col">
                                            <div class="product-item mb-20">
                                                <div class="product-thumb">
                                                    <a href="product-details.html">
                                                        <img src="melani/assets/img/product/product-ac1.jpg" alt="product image">
                                                    </a>
                                                    <div class="box-label">
                                                        <?php if($productoNuevo->new == 1){ ?>
                                                            <div class="product-label new">
                                                                <span>new</span>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if($productoNuevo->discount > 0){ ?>
                                                            <div class="product-label discount">
                                                                <span>-5%</span>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="product-action-link">
                                                        <a href="#" data-toggle="modal" data-target="#quick_view"> <span
                                                            data-toggle="tooltip" data-placement="left" title="Quick view"><i class="ion-ios-eye-outline"></i></span> </a>
                                                        <a href="#" data-toggle="tooltip" data-placement="left" title="Compare"><i
                                                            class="ion-ios-loop"></i></a>
                                                        <a href="#" data-toggle="tooltip" data-placement="left" title="Wishlist"><i
                                                            class="ion-ios-shuffle"></i></a>
                                                    </div>
                                                </div>
                                                <div class="product-description text-center">
                                                    <div class="manufacturer">
                                                        <p><a href="product-details.html"><?= $productoNuevo->brand ?></a></p>
                                                    </div>
                                                    <div class="product-name">
                                                        <h3><a href="product-details.html"><?= $productoNuevo->name  ?></a></h3>
                                                    </div>
                                                    <div class="price-box">
                                                        <?php if($productoNuevo->discount > 0 ){ ?>
                                                            <span class="regular-price">$<?= ($productoNuevo->price * (100 - $productoNuevo->discount))/100 ?></span>
                                                                <span class="old-price"><del>$<?= $productoNuevo->price  ?></del></span>

                                                        <?php }else{ ?>
                                                            <span class="regular-price">$<?= $productoNuevo->price ?></span>
                                                        <?php } ?>
                                                    </div>

                                                    <div class="hover-box text-center">
                                                        <div class="ratings">
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="best-seller-area mt-md-90 mt-sm-48">
                            <div class="section-title-2 mb-60 mb-sm-40">
                                <h3>best seller</h3>
                            </div>
                            <div class="category-active row slick-arrow-style spt arrow-top">
                                <?php if(!empty($productosMasVendidos)){ ?>
                                    <?php foreach ($productosMasVendidos as $key => $producto) { ?>
                                        <div class="col">
                                            <div class="product-item mb-20">
                                                <div class="product-thumb">
                                                    <a href="product-details.html">
                                                        <img src="melani/assets/img/product/product-ac1.jpg" alt="product image">
                                                    </a>
                                                    <div class="box-label">
                                                        <?php if($producto->new == 1){ ?>
                                                            <div class="product-label new">
                                                                <span>new</span>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if($producto->discount > 0){ ?>
                                                            <div class="product-label discount">
                                                                <span>-5%</span>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="product-action-link">
                                                        <a href="#" data-toggle="modal" data-target="#quick_view"> <span
                                                            data-toggle="tooltip" data-placement="left" title="Quick view"><i class="ion-ios-eye-outline"></i></span> </a>
                                                        <a href="#" data-toggle="tooltip" data-placement="left" title="Compare"><i
                                                            class="ion-ios-loop"></i></a>
                                                        <a href="#" data-toggle="tooltip" data-placement="left" title="Wishlist"><i
                                                            class="ion-ios-shuffle"></i></a>
                                                    </div>
                                                </div>
                                                <div class="product-description text-center">
                                                    <div class="manufacturer">
                                                        <p><a href="product-details.html"><?= $producto->brand ?></a></p>
                                                    </div>
                                                    <div class="product-name">
                                                        <h3><a href="product-details.html"><?= $producto->name  ?></a></h3>
                                                    </div>
                                                    <div class="price-box">
                                                        <?php if($producto->discount > 0 ){ ?>
                                                            <span class="regular-price">$<?= ($producto->price * (100 - $producto->discount))/100 ?></span>
                                                                <span class="old-price"><del>$<?= $producto->price  ?></del></span>

                                                        <?php }else{ ?>
                                                            <span class="regular-price">$<?= $producto->price ?></span>
                                                        <?php } ?>
                                                    </div>

                                                    <div class="hover-box text-center">
                                                        <div class="ratings">
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                            <span><i class="fa fa-star"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- catrgory product area end -->

        <!-- banner feature start -->
        <div class="banner-feature-area theme-color-4 pt-62 pb-60 pt-sm-56 pb-sm-20">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="banner-single-feature text-center mb-sm-30">
                            <i class="ion-paper-airplane"></i>
                            <h4>FREE SHIPPING & DELIVERY</h4>
                            <p class="text-white">We’re one of the furniture online retailers, who offer free of charge delivery</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="banner-single-feature text-center mb-sm-30">
                            <i class="ion-ios-time-outline"></i>
                            <h4>365-DAY HOME TRIAL</h4>
                            <p class="text-white">Our unique return policy will allow you to return furniture for almost a year</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="banner-single-feature text-center mb-sm-30">
                            <i class="ion-trophy"></i>
                            <h4>LIFETIME WARRANTY</h4>
                            <p class="text-white">Purchasing furniture with us comes with warranty, than anyone else offers!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner feature end -->

    <?php }else{ ?>
        <img style="height: 100%; width: 100%;" src="images/contruccion_pagina.png">
    <?php } ?>


@stop
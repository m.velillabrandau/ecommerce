@extends('layout')

@section('content')



<style>


</style>

<script src="{{ asset('js/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"></script>

<link rel="stylesheet" href="{{ asset('js/sweetalert2/dist/sweetalert2.min.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Content Header (Page header) -->
<section class="content-header" index="1">
    <h1>
        PRODUCTOS 
        <small>Optional description</small>
    </h1>
    <div class="breadcrumb">
      {{--   <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li> --}}
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalAgregarProducto">Agregar Productos</button>
    </div>
</section>

<!-- Main content -->
<section class="content container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Hover Data Table</h3>
                </div>
                <!-- /.box-header -->
 
                <div class="box-body">

                  <table class="table table-bordered table-striped" id="tabla-planillas">
                    <thead>
                      <tr>
                        <th class="text-center">IMAGEN</th>
                        <th class="text-center">NOMBRE</th>
                        <th class="text-center">MARCA</th>
                        <th class="text-center">STOCK</th>
                        <th class="text-center">NUEVO</th>
                        <th class="text-center">MAS VENDIDO</th>
                        <th class="text-center">DESCUENTO</th>
                        <th class="text-center">PRECIO</th>
                        <th class="text-center">ACTIVO</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($productos as $key => $producto) { ?>
                        <tr>
                          <td style="width: 85px; height: 95px;" class="">
                            <?php if($producto->file != '' || $producto->file != null){ ?>
                              <img style="height: 100%; width: 100%;" src="images/products/<?= $producto->file ?>" alt="product image">
                            <?php }else { ?>
                              <img style="height: 100%; width: 100%;" src="melani/assets/img/product/product-1.jpg" alt="product image">
                            <?php } ?>
                          </td>
                          <td class="text-center"><?= $producto->name ?></td>
                          <td class="text-center"><?= $producto->brand ?></td>
                          <td class="text-center"><?= $producto->stock?></td>
                          <td class="text-center"><?= $producto->new ?></td>
                          <td class="text-center"><?= $producto->best_seller ?></td>
                          <td class="text-center"><?= $producto->discount ?></td>
                          <td class="text-center"><?= $producto->price ?></td>
                          <td class="text-center"><?= $producto->active ?></td>
                          <td class="text-center">
                            <div class="btn-group">
                              <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a class="" data-toggle="modal" data-target="#modalEditarPlanilla">Editar</a></li>
                                  <li><a class="eliminar_producto" id-producto="<?= $producto->id ?>" >Eliminar</li>
                              </ul>
                           </div>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>

                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>


<!-- MODAL -->
<div id="modalAgregarProducto" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">AGREGAR PRODUCTO</h4>
			<p>Click on this paragraph</p>
		</div>
        <form id="form_producto" action="" method="POST" enctype="multipart/form-data">
          	<div class="modal-body">

              <div class="row">
                <div class="col-md-4">
                  <div class="form-group has-feedback">
                    <label for="nombreProducto">Nombre</label>
                    <input type="text"  class="form-control"  placeholder="Nombre" name="nombreProducto">
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group has-feedback">
                    <label for="marcaProducto">Marca</label>
                    <input type="text"  class="form-control"  placeholder="Marca" name="marcaProducto">
                  </div>
                </div>

                <div class="col-md-2">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1" name="nuevoProducto">
                        <label class="form-check-label" for="exampleCheck1">¿ Nuevo ?</label>
                    </div>
                </div>

              </div>

              <div class="row">
                <div class="col-md-2">
                    <div class="form-group has-feedback">
                        <label for="stockProducto">Stock</label>
                        <input type="text"  class="form-control"  placeholder="Stock" name="stockProducto">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group has-feedback">
                        <label for="precioProducto">Precio</label>
                        <input type="text"  class="form-control"  placeholder="Precio" name="precioProducto">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group has-feedback">
                        <label for="descuentoProducto">Descuento</label>
                        <input type="text"  class="form-control"  placeholder="Precio" name="descuentoProducto">
                    </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Example file input</label>
                        <input type="file" id="img-producto" name="img-producto">
                    </div>
                </div>
              </div>

          	</div>
          	<div class="modal-footer">
            	<button type="button" class="btn btn-danger" id="closeModal" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success" id="">Agregar</button>
          	</div>
        </form>
    </div>

  </div>
</div>

<script type="text/javascript"></script>

<script>
    $(function () {

        

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('#form_producto').on('submit', function(event){

            event.preventDefault();
            //AJAX PARA ...
            $.ajax({
                url: "{{ route('agregarProducto') }}",
                type: 'POST',
                data: new FormData(this),
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success:  false
            })
            .done(function(response) {

                if(response.status == 1){

                    $('#modalAgregarProducto').modal('hide');

                    Swal({
                      title: response.msg,
                      type: 'success',
                      confirmButtonColor: '#3085d6',
                      confirmButtonText: 'OK'
                    }).then((result) => {
                      if (result.value) {
                        location.reload();
                      }
                    })
                }
                else{

                    $('#modalAgregarProducto').modal('hide');

                    Swal({
                      title: response.msg,
                      type: 'error',
                      confirmButtonText: 'Ok'
                    })
                }

            })
            .fail(function(response) {
                // Swal({
                //       title: 'Error',
                //       type: 'error',
                //       confirmButtonText: 'Cool'
                //     })
            }); 
        });

        //Script para eliminar un producto
        $( ".eliminar_producto" ).on( "click", function() {

            Swal({
                title: '¿ Estas seguro de eliminar el producto?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si.'
            }).then((result) => {
                if (result.value) {

                    var id_producto = this.getAttribute("id-producto");

                    //AJAX PARA ELIMINAR PRODUCTO 
                    $.ajax({
                        url: "{{ route('eliminarProducto') }}",
                        type: 'POST',
                        data:{
                            id_producto: id_producto
                        }
                    })
                    .done(function(response) {
                        if(response.status == 1){
                            Swal({
                              title: response.msg,
                              type: 'success',
                            }).then((result) => {
                                if (result.value) {
                                    location.reload();
                                }
                            })
                        }else{
                            Swal({
                              title:  response.msg,
                              type: 'error',
                            })
                        }
                    })
                    .fail(function(response) {
                        Swal({
                          title: 'Error!',
                          type: 'error',
                        })
                    }); 
                }
                else{

                }
            })
        });

    });
</script>

@stop


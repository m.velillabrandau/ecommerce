<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">

    <!-- Site title -->
    <title>Lucete</title>
    <!-- Favicon -->
    {{-- <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon" /> --}}
    <link rel="shortcut icon" href="{{ asset('melani/assets/img/favicon.icon') }}" type="image/x-icon">
    <!-- Bootstrap CSS -->
    {{-- <link href="assets/css/bootstrap.min.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ asset('melani/assets/css/bootstrap.min.css') }}">
    <!-- Font-Awesome CSS -->
    {{-- <link href="assets/css/font-awesome.min.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ asset('melani/assets/css/font-awesome.min.css') }}">
    <!-- IonIcon CSS -->
    {{-- <link href="assets/css/ionicons.min.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ asset('melani/assets/css/ionicons.min.css') }}">
    <!-- helper class css -->
    {{-- <link href="assets/css/helper.min.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ asset('melani/assets/css/helper.min.css') }}">
    <!-- Plugins CSS -->
    {{-- <link href="assets/css/plugins.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ asset('melani/assets/css/plugins.css') }}">
    <!-- Main Style CSS -->
    {{-- <link href="assets/css/style-downy.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ asset('melani/assets/css/style-downy.css') }}">

</head>

<body>


    <!-- header area start -->
    <header>

        <!-- main menu area start -->
        <div class="header-main sticky pt-sm-10 pb-sm-10 pt-md-10 pb-md-10">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-6 col-6">
                        <div class="logo">
                            <a href="{{ route('indexWeb') }}">
                                <img style="border-radius: 50%; height: 80px; width: 80px;" src="images/logoLucete.jpeg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 d-none d-lg-block">
                        <div class="main-header-inner">
                            <div class="main-menu">
                                <nav id="mobile-menu">
                                    <ul>
                                        <li class="active"><a href="{{ route('indexWeb') }}">Home</a>

                                        </li>
                                        <li><a href="{{ route('shop') }}">Tienda</a>

                                        </li>
                                        <li><a href="contact-us.html">Contacto</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6 ml-auto">
                        <div class="header-setting-option">
                            <div class="search-wrap">
                                <button type="submit" class="search-trigger"><i class="ion-ios-search-strong"></i></button>
                            </div>
{{--                             <div class="header-mini-cart">
                                <div class="mini-cart-btn">
                                    <i class="ion-bag"></i>
                                    <span class="cart-notification">2</span>
                                </div>
                                <ul class="cart-list">
                                    <li>
                                        <div class="cart-img">
                                            <a href="product-details.html"><img src="melani/assets/img/cart/cart-1.jpg"
                                                    alt=""></a>
                                        </div>
                                        <div class="cart-info">
                                            <h4><a href="product-details.html">simple product 09</a></h4>
                                            <span>$60.00</span>
                                        </div>
                                        <div class="del-icon">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart-img">
                                            <a href="product-details.html"><img src="melani/assets/img/cart/cart-2.jpg"
                                                    alt=""></a>
                                        </div>
                                        <div class="cart-info">
                                            <h4><a href="product-details.html">virtual product 10</a></h4>
                                            <span>$50.00</span>
                                        </div>
                                        <div class="del-icon">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </li>
                                    <li class="mini-cart-price">
                                        <span class="subtotal">subtotal : </span>
                                        <span class="subtotal-price ml-auto">$110.00</span>
                                    </li>
                                    <li class="checkout-btn">
                                        <a href="#">checkout</a>
                                    </li>
                                </ul>
                            </div> --}}
                            {{-- <div class="settings-top">
                                <div class="settings-btn">
                                    <i class="ion-android-settings"></i>
                                </div>
                                <ul class="settings-list">
                                    <li>
                                        English <i class="fa fa-angle-down"></i>
                                        <ul>
                                            <li class="active"><a href="#">english</a></li>
                                            <li><a href="#">Français</a></li>
                                            <li><a href="#">Germany</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        USD $ <i class="fa fa-angle-down"></i>
                                        <ul>
                                            <li><a href="#"> € Euro</a></li>
                                            <li class="active"><a href="#"> $ US Dollar</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        my account <i class="fa fa-angle-down"></i>
                                        <ul>
                                            <li><a href="#">my account</a></li>
                                            <li><a href="#">login</a></li>
                                            <li><a href="#">register</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div> --}}
                        </div>
                    </div>
                    <div class="col-12 d-block d-lg-none">
                        <div class="mobile-menu"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main menu area end -->

        <!-- Start Search Popup -->
        <div class="box-search-content search_active block-bg close__top">
            <form class="minisearch" action="#">
                <div class="field__search">
                    <input type="text" placeholder="Search Our Catalog">
                    <div class="action">
                        <a href="#"><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </form>
            <div class="close__wrap">
                <span>close</span>
            </div>
        </div>
        <!-- End Search Popup -->

    </header>
    <!-- header area end -->

        @yield('contentweb')

    <footer>
        <!-- newsletter area start -->
        <div class="newsletter-area bg-gray pt-64 pb-64 pt-sm-56 pb-sm-58">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        {{-- <div class="newsletter-inner">
                            <div class="newsletter-title">
                                <h3>newsletter signup</h3>
                            </div>
                            <div class="newsletter-box">
                                <form id="mc-form">
                                    <input type="email" id="mc-email" autocomplete="off" placeholder="Your Email address">
                                    <button class="newsletter-btn" id="mc-submit"><i class="ion-android-send"></i></button>
                                </form>
                            </div>
                        </div>
                        <!-- mailchimp-alerts Start -->
                        <div class="mailchimp-alerts">
                            <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                            <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                            <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                        </div> --}}
                        <!-- mailchimp-alerts end -->
                    </div>
                    <div class="col-lg-6 col-md-6 ml-auto">
                        <div class="social-share-area">
                            <h3> Síguenos</h3>
                            <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                {{-- <a href="#"><i class="fa fa-twitter"></i></a> --}}
                                {{-- <a href="#"><i class="fa fa-rss"></i></a> --}}
                                {{-- <a href="#"><i class="fa fa-youtube"></i></a> --}}
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- newsletter area end -->

        <!-- footer widget area start -->
        <div class="footer-widget-area pt-62 pb-56 pb-md-26 pt-sm-56 pb-sm-20">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="footer-widget">
                            <div class="footer-widget-title">
                                <h3>Compra y delivery</h3>
                            </div>
                            <div class="footer-widget-body">
                                {{-- <p>Here you can read some details about a nifty little lifecycle of your order's journey from the time you place your order to your new treasures arriving at your doorstep.</p> --}}
                            </div>
                            <div class="footer-widget-title mt-20">
                                <h3>Metodos de pago</h3>
                            </div>
                            <div class="footer-widget-body">
                                {{-- <p>It is equally important to choose the solution which offers a specific selection of credit cards. We take Visa & MasterCard as they are widely used by cyber customers.</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6">
                        <div class="footer-widget">
                            {{-- <div class="footer-widget-title">
                                <h3>useful link</h3>
                            </div>
                            <div class="footer-widget-body">
                                <ul class="useful-link">
                                    <li><a href="#">Delivery</a></li>
                                    <li><a href="#">Legal Notice</a></li>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">New products</a></li>
                                    <li><a href="#">best sales</a></li>
                                    <li><a href="#">wishlist</a></li>
                                    <li><a href="#">my account</a></li>
                                </ul>
                            </div> --}}
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6">
                        <div class="footer-widget">
                            {{-- <div class="footer-widget-title">
                                <h3>our company</h3>
                            </div>
                            <div class="footer-widget-body">
                                <ul class="useful-link">
                                    <li><a href="#">Delivery</a></li>
                                    <li><a href="#">Legal Notice</a></li>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">secure payment</a></li>
                                    <li><a href="#">contact us</a></li>
                                    <li><a href="#">site map</a></li>
                                    <li><a href="#">login</a></li>
                                </ul>
                            </div> --}}
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="footer-widget">
                            <div class="footer-widget-title">
                                <div class="footer-logo">
                                    <a href="index-4.html">
                                        <img style="border-radius: 50%; height: 80px; width: 80px;" src="images/logoLucete.jpeg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="footer-widget-body">
                                <ul class="address-box">
                                    {{-- <li>
                                        <span>ADDRESS:</span>
                                        <p>Melani - Responsive Prestashop Theme<br>
                                        169-C, Technohub, Dubai Silicon</p>
                                    </li> --}}
                                    <li>
                                        <span>Consulta</span>
                                        <p>+56965685584</p>
                                    </li>
                                    <li>
                                        <span>correo:</span>
                                        <p>mathias.velilla@gmail.com</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer widget area end -->

        <!-- footer botton area start -->
        <div class="footer-bottom-area">
            <div class="container">
                <div class="bdr-top pt-18 pb-18">
                    <div class="row align-items-center">
                        <div class="col-md-6 order-2 order-md-1">
                            <div class="copyright-text">
                                <p>copyright <a href="#">HasTech</a>. All Rights Reserved</p>
                            </div>
                        </div>
                        {{-- <div class="col-md-6 ml-auto order-1 order-md-2">
                            <div class="footer-payment">
                                <img src="melani/assets/img/payment.png" alt="">
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
        <!-- footer botton area end -->

    </footer>
    <!-- footer area end -->


    <!-- Quick view modal start -->
    <div class="modal" id="quick_view">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <!-- product details inner end -->
                    <div class="product-details-inner">
                        <div class="row">
                            <div class="col-lg-5 col-md-5">
                                <div class="product-large-slider mb-20 slider-arrow-style slider-arrow-style__style-2">
                                    <div class="pro-large-img">
                                        <img src="melani/assets/img/product/product-details-img1.jpg" alt="" />
                                    </div>
                                    <div class="pro-large-img">
                                        <img src="melani/assets/img/product/product-details-img2.jpg" alt=""/>
                                    </div>
                                    <div class="pro-large-img">
                                        <img src="melani/assets/img/product/product-details-img3.jpg" alt=""/>
                                    </div>
                                    <div class="pro-large-img">
                                        <img src="melani/assets/img/product/product-details-img4.jpg" alt=""/>
                                    </div>
                                    <div class="pro-large-img">
                                        <img src="melani/assets/img/product/product-details-img4.jpg" alt=""/>
                                    </div>
                                </div>
                                <div class="pro-nav slick-padding2 slider-arrow-style slider-arrow-style__style-2">
                                    <div class="pro-nav-thumb"><img src="melani/assets/img/product/product-details-img1.jpg" alt="" /></div>
                                    <div class="pro-nav-thumb"><img src="melani/assets/img/product/product-details-img2.jpg" alt="" /></div>
                                    <div class="pro-nav-thumb"><img src="melani/assets/img/product/product-details-img3.jpg" alt="" /></div>
                                    <div class="pro-nav-thumb"><img src="melani/assets/img/product/product-details-img4.jpg" alt="" /></div>
                                    <div class="pro-nav-thumb"><img src="melani/assets/img/product/product-details-img5.jpg" alt="" /></div>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7">
                                <div class="product-details-des pt-sm-30">
                                    <h3>Chaz Kangeroo Hoodies</h3>
                                    <div class="ratings">
                                        <span class="good"><i class="fa fa-star"></i></span>
                                        <span class="good"><i class="fa fa-star"></i></span>
                                        <span class="good"><i class="fa fa-star"></i></span>
                                        <span class="good"><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <div class="pro-review">
                                            <span><a href="#">1 review(s)</a></span>
                                        </div>
                                    </div>
                                    <div class="pricebox">
                                        <span class="regular-price">$160.00</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.<br>
                                    Phasellus id nisi quis justo tempus mollis sed et dui. In hac habitasse platea dictumst.</p>
                                    <div class="quantity-cart-box d-flex align-items-center mb-24">
                                        <div class="quantity">
                                            <div class="pro-qty"><input type="text" value="1"></div>
                                        </div>
                                        <div class="product-btn product-btn__color">
                                            <a href="#"><i class="ion-bag"></i>Add to cart</a>
                                        </div>
                                    </div>
                                    <div class="availability mb-16">
                                        <h5>Availability:</h5>
                                        <span>in stock</span>
                                    </div>
                                    <div class="share-icon">
                                        <h5>share:</h5>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-pinterest"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- product details inner end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Quick view modal end -->


    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->


    <!--All jQuery, Third Party Plugins & Activation (main.js) Files-->
    <!-- <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script> -->
    <script src="{{ asset('melani/assets/js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <!-- Jquery Min Js -->
    <!-- <script src="assets/js/vendor/jquery-3.3.1.min.js"></script> -->
    <script src="{{ asset('melani/assets/js/vendor/jquery-3.3.1.min.js') }}"></script>
    <!-- Popper Min Js -->
    ç
    <script src="{{ asset('melani/assets/js/vendor/popper.min.js') }}"></script>
    <!-- Bootstrap Min Js -->
    <!-- <script src="assets/js/vendor/bootstrap.min.js"></script> -->
    <script src="{{ asset('melani/assets/js/vendor/bootstrap.min.js') }}"></script>
    <!-- Plugins Js-->
    <!-- <script src="assets/js/plugins.js"></script> -->
    <script src="{{ asset('melani/assets/js/plugins.js') }}"></script>
    <!-- Ajax Mail Js -->
    <!-- <script src="assets/js/ajax-mail.js"></script> -->
    <script src="{{ asset('melani/assets/js/ajax-mail.js') }}"></script>
    <!-- Active Js -->
    <!-- <script src="assets/js/main.js"></script> -->
    <script src="{{ asset('melani/assets/js/main.js') }}"></script>

</body>

</html>
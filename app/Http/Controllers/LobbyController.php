<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Products;

class LobbyController extends Controller
{

	public function index()
	{	
		$return = [];

		$products = DB::table('products')
			->where([
				['products.active','=',1]
			])
			->get();

		//dd($products);


		$return ['status'] = true;
		$return ['msg'] = 'Exito';

		return view('lobby.index',[
                'productos' => $products
            ]);
	}

	public function agregarProducto(Request $request)
	{

		$return = [];
    	$data = $request->all();
    	
    	//dd($path);

    	if (!empty($data)) {

    		$image = $request->file('img-producto');
    		$new_name = rand().'.'.$image->getClientOriginalExtension();
    		//dd($new_name);
    		$image->move(public_path("images/products"), $new_name);
    		//dd($path);

    		$producto = new Products;
    		$producto->name = $data['nombreProducto'];
    		$producto->brand = $data['marcaProducto'];
    		$producto->description = $data['nombreProducto'];
    		$producto->file = $new_name;
    		$producto->stock = (int)$data['stockProducto'];   
            if(isset($data['nuevoProducto'])){
                $producto->new = 1;
            }else{
                $producto->new = 0;
            }
            $producto->featured = 1;
    		$producto->best_seller = 0;
    		$producto->discount = (int)$data['descuentoProducto'];
    		$producto->price = (int)$data['precioProducto'];
    		$producto->active = 1;

    		if($producto->save()){
    			http_response_code(200);
    			$return ['status'] = true;
				$return ['msg'] = 'Exito al agregar producto';
    		}else{
    			http_response_code(500);
    			$return ['status'] = false;
				$return ['msg'] = 'Error al agregar producto';
    		}

    	}else{
    		http_response_code(500);
    		$return ['status'] = false;
			$return ['msg'] = 'No existen datos de entrada.';
    	}
    	
    	
    	

    	return $return;
    }

    public function eliminarProducto(Request $request)
    {

        $return = [];
        $data = $request->all(); // This will get all the request data.

        if($data['id_producto'] != '' || $data['id_producto'] != null){

            $borrado_producto = DB::table('products')->where([
                        ['products.id','=', $data['id_producto']]
                    ])
                    ->delete();

            if($borrado_producto == 1){

                $return['status'] = $borrado_producto;
                $return['msg'] = 'Exito al eliminar el producto';

            }else{
                $return['status'] = $borrado_producto;
                $return['msg'] = 'Error al eliminar';
            }
        }

        return $return;
    }



}
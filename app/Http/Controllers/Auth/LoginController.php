<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class LoginController extends Controller
{
	public function __construct()
	{
		$this->middleware('guest',['only' => 'showLoginForm']);
	}


	public function showLoginForm()
	{
		return view('auth.login');
	}


	public function login(Request $request)
	{
		//dd($request->password);
		$this->validate($request, [
			'email' => 'email|required|string',
			'password' => 'required|string'
		]);

		//$encrypted = Crypt::encryptString($request->password);
		//dd($encrypted);
		if(Auth::attempt( ['email' => $request->email, 'password' => $request->password] ))
		{
			return redirect()->route('lobby');
		}

		return back()
                ->withErrors(['email' => 'Estas credenciales no coinciden con nuestros registros'])
                ->withInput(request(['email']));
	}

	public function logout()
	{
		Auth::logout();

		return redirect('/');
	}
}

<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Products;


class WebController extends Controller
{

	public function index()
	{

		$productsFeatured = $this->getProductsFeatured();
		$productsNew = $this->getProductsNew();
		$bestSellerProducts = $this->getBestSellerProducts();
		//dd($productsNew);

		//dd($products);

		return view('web.index',[
			'productos' => $productsFeatured,
			'productosNuevos' => $productsNew,
			'productosMasVendidos' => $bestSellerProducts
        ]);



	}

	public function shop(Request $request)
	{
		$return = [];
		$data = $request->all();
		//dd($data['rank']);
		if((!empty($data['rankPrice'])) && (!empty($data['category']))){ //Estan los dos filtros presentes

			$category = $data['category'];
			$ranks = explode("-", $data['rankPrice']);
			// dd($ranks);
			$minRank = $ranks[0];
			$maxRank = $ranks[1];

			if($maxRank = '+'){

				$products = DB::table('products')
					->where('active', '=', 1)
					->where('price', '>=', $minRank);
			}else{

				$categoryProduct = DB::table('products')
					->where('active', '=', 1)
					->where('price', '>=', $minRank)
					->where('price', '<=', $maxRank);

				$products = DB::table('products')
					->where('active', '=', 1)
					->where('price', '>=', $minRank)
					->where('price', '<=', $maxRank);
			}
			
		}

		if((!empty($data['rankPrice'])) && (empty($data['category']))){ //Esta SOLO el filtro del rango de precio

			$ranks = explode("-", $data['rankPrice']);
			// dd($ranks);
			$minRank = $ranks[0];
			$maxRank = $ranks[1];

			if($maxRank == '+'){

				$products = DB::table('products')
					->where('active', '=', 1)
					->where('price', '>=', $minRank);
			}else{

				$products = DB::table('products')
					->where('active', '=', 1)
					->where('price', '>=', $minRank)
					->where('price', '<=', $maxRank);
			}

			//dd($products);

		}

		if((empty($data['rankPrice'])) && (!empty($data['category']))){ // Esta SOLO el filtro de la categoria

			$category = $data['category'];

            $products = DB::table('products_categories')
	            ->join('products', 'products_categories.id_product', '=', 'products.id')
	            ->where('id_category', '=', $category)
	            ->where('products.active', '=', 1)
	            ->where('products_categories.active', '=', 1);

	       //dd($products);
		}

		if( (empty($data['rankPrice'])) and (empty($data['category'])) ){ // No esta ningun filtro
			$products = DB::table('products')
				->where('active', '=', 1);
		}

		if( (!empty($data['order'])) ){

			$order = $data['order'];

			$products = $products->orderBy('products.price', $order);

			//dd($products);
		}

		$products = $products->simplePaginate(6);

		return view('web.shop',[
			'productos' => $products
        ]);
        
	}


	public function getProductsFeatured()
	{
		$productsFeatured = DB::table('products')
			->where('featured', '=', 1)
			->where('active', '=', 1)
            ->get();

		return $productsFeatured;
	}

	public function getProductsNew()
	{
		$productsNew = DB::table('products')
			->where('new', '=', 1)
			->where('active', '=', 1)
            ->get();

		return $productsNew;
	}

	public function getBestSellerProducts()
	{
		$bestSellerProducts = DB::table('products')
			->where('best_seller', '=', 1)
			->where('active', '=', 1)
            ->get();

		return $bestSellerProducts;
	}


}
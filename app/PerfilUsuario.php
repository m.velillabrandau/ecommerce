<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PerfilUsuario extends Model
{   
    
    protected $table = 'perfiles_usuarios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'id_usuario', 'id_perfil', 'active'];

     public function prueba2()
    {
        return $this->belongsToMany('App\Usuario');
    }

}

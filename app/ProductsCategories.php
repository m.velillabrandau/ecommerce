<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ProductsCategories extends Model
{   
    
    protected $table = 'products_categories';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'brand', 'description', 'file', 'stock', 'new', 'featured', 'best_seller', 'discount', 'price', 'active'];

}
